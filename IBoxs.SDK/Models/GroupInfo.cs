﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBoxs.SDK.Models
{
    public class GroupInfo
    {
        /// <summary>
        /// 群号
        /// </summary>
        public long Group {private set; get; }

        /// <summary>
        /// 群名称
        /// </summary>
        public string GroupName { private set; get; }

        public GroupInfo(long group,string name)
        {
            this.Group = group;
            this.GroupName = name;
        }
    }
}
