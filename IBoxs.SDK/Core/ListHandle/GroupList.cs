﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IBoxs.SDK.Models;
using Newtonsoft.Json;

namespace IBoxs.SDK.Core.ListHandle
{
    class GroupList
    {
        /// <summary>
        /// 获取群列表信息
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static List<GroupInfo> GetGroupList(string json)
        {
            List<GroupInfo> groupList = new List<GroupInfo>();
            if (!json.Contains("\"gn\":")) return groupList;
            Root rt = JsonConvert.DeserializeObject<Root>(json);
            if (rt.errcode != 0)
                return groupList;
            if (json.Contains("\"create\":"))
            {
                for (int i = 0; i < rt.create.Count; i++) {
                    GroupInfo g = new GroupInfo(rt.create[i].gc, rt.create[i].gn);
                    groupList.Add(g);
                }
            }
            if (json.Contains("\"manage\":"))
            {
                for (int i = 0; i < rt.manage.Count; i++)
                {
                    GroupInfo g = new GroupInfo(rt.manage[i].gc, rt.manage[i].gn);
                    groupList.Add(g);
                }
            }
            if (json.Contains("\"join\":"))
            {
                for (int i = 0; i < rt.join.Count; i++)
                {
                    GroupInfo g = new GroupInfo(rt.join[i].gc, rt.join[i].gn);
                    groupList.Add(g);
                }
            }
            return groupList;
        }

        public class ManageItem
        {
            /// <summary>
            /// 
            /// </summary>
            public long gc { get; set; }
            /// <summary>
            /// 讨论组测试
            /// </summary>
            public string gn { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public long owner { get; set; }
        }

        public class JoinItem
        {
            /// <summary>
            /// 
            /// </summary>
            public long gc { get; set; }
            /// <summary>
            /// 讨论组测试
            /// </summary>
            public string gn { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public long owner { get; set; }
        }

        public class CreateItem
        {
            /// <summary>
            /// 
            /// </summary>
            public long gc { get; set; }
            /// <summary>
            /// 讨论组测试
            /// </summary>
            public string gn { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public long owner { get; set; }
        }

        public class Root
        {
            /// <summary>
            /// 
            /// </summary>
            public int ec { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int errcode { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string em { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public List<ManageItem> manage { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public List<JoinItem> join { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public List<CreateItem> create { get; set; }
        }
    }
}
