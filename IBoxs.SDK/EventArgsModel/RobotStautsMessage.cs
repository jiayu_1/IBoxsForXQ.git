﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IBoxs.SDK.Enum;

namespace IBoxs.SDK.EventArgsModel
{
    /// <summary>
    /// 机器人QQ状态消息
    /// </summary>
   public class XqRobotStautsMessage
    {
        /// <summary>
        /// 机器人QQ
        /// </summary>
        public long RobotQQ { get; private set; }
        /// <summary>
        /// 状态变化情况
        /// </summary>
        public RobotQQStautsEnum Type { get; private set; }

        /// <summary>
        /// 是否截断
        /// </summary>
        public MessageTypeEnum Handler { get; set; }


        public XqRobotStautsMessage(long robotQQ, int Type)
        {
            this.RobotQQ = robotQQ;
            this.Type = (RobotQQStautsEnum)Type;
            this.Handler = (MessageTypeEnum)0;
        }
    }
}
