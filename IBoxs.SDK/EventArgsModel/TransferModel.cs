﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IBoxs.SDK.Enum;

namespace IBoxs.SDK.EventArgsModel
{
    /// <summary>
    /// 财付通转账消息
    /// </summary>
   public class XqTransferModel
    {
        /// <summary>
        /// 机器人QQ
        /// </summary>
        public long RobotQQ { get; set; }

        /// <summary>
        /// 获取当前消息的来源QQ号
        /// </summary>
        public long FromQQ { get; private set; }

        /// <summary>
        /// 转账来源
        /// </summary>
        public TransferEnum Type { get; private set; }

        /// <summary>
        /// 获取当前消息的消息内容
        /// </summary>
        public string Json { get; private set; }

        /// <summary>
        /// 是否截断
        /// </summary>
        public MessageTypeEnum Handler { get; set; }


        /// <summary>
        /// 初始化 <see cref="XqGroupMessageModel"/> 类的一个新实例
        /// </summary>
        /// <param name="msgId">消息ID</param>
        /// <param name="fromGroup">来源群组</param>
        /// <param name="fromQQ">来源QQ</param>
        /// <param name="anonymous">来源匿名</param>
        /// <param name="msg">消息</param>
        public XqTransferModel(long robotQQ, long fromQQ,int type, string json)
        {
            this.RobotQQ = robotQQ;
            this.FromQQ = fromQQ;
            this.Type =(TransferEnum)type;
            this.Json = json;
            this.Handler = (MessageTypeEnum)0;
        }
    }
}
