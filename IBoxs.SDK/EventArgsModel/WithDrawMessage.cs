﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IBoxs.SDK.Enum;

namespace IBoxs.SDK.EventArgsModel
{
    /// <summary>
    /// 撤回事件消息
    /// </summary>
   public class XqWithDrawMessage
    {
        /// <summary>
        /// 机器人QQ
        /// </summary>
        public long RobotQQ { get; set; }

        /// <summary>
        /// 获取当前消息的来源群
        /// </summary>
        public long FromGroup { get; private set; }
        /// <summary>
        /// 获取当前消息的来源QQ号
        /// </summary>
        public long FromQQ { get; private set; }
        /// <summary>
        /// 撤回消息子类型
        /// </summary>
        public WithdrawEnum Type { get; set; }
        /// <summary>
        /// 消息ID
        /// </summary>
        public string MsgID { get; private set; }
        /// <summary>
        /// 消息序号
        /// </summary>
        public string MsgNum { get; private set; }

        /// <summary>
        /// 是否截断
        /// </summary>
        public MessageTypeEnum Handler { get; set; }
        
        public XqWithDrawMessage(long robotQQ,long fromGroup, long fromQQ, int type,string msgID,string msgNum)
        {
            this.RobotQQ = robotQQ;
            this.FromQQ = fromQQ;
            this.Type = (WithdrawEnum)type;
            this.FromGroup = FromGroup;
            this.MsgID = msgID;
            this.MsgNum = msgNum;
            this.Handler = (MessageTypeEnum)0;
        }
    }
}
