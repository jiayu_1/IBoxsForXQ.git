﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IBoxs.SDK.Enum;

namespace IBoxs.SDK.EventArgsModel
{
    /// <summary>
    /// 请求入群信息
    /// </summary>
   public class XqAddGroupRequest
    {
        /// <summary>
        /// 机器人QQ
        /// </summary>
        public long RobotQQ { get; private set; }
        /// <summary>
        /// 来源群
        /// </summary>
        public long FromGroup { get; private set; }

        /// <summary>
        /// 来源QQ
        /// </summary>
        public long FromQQ { get; set; }

        /// <summary>
        /// 是否为不良群成员
        /// </summary>
        public bool Type { get; set; }

        /// <summary>
        /// 进群备注消息
        /// </summary>
        public string Note { get; private set; }

        /// <summary>
        /// 进群原始seq
        /// </summary>
        public string Seq { get; private set; }
        /// <summary>
        /// 是否截断
        /// </summary>
        public MessageTypeEnum Handler { get; set; }

        public XqAddGroupRequest(long robotQQ, long fromGroup, long fromQQ,string note,string Seq, int type)
        {
            this.RobotQQ = robotQQ;
            this.FromGroup = fromGroup;
            this.FromQQ = FromQQ;
            this.Type = type > 0;
            this.Note = note;
            this.Seq = Seq;
            this.Handler = (MessageTypeEnum)0;
        }
    }
}
