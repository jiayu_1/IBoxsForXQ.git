﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IBoxs.SDK.Enum;

namespace IBoxs.SDK.EventArgsModel
{
    /// <summary>
    /// 群私聊消息
    /// </summary>
   public class XqGroupPrivateMessageModel
    {
        /// <summary>
        /// 获取或设置一个值, 表示当前事件所产生消息的唯一编号, 可用于撤回消息
        /// </summary>
        public string MsgId { get; set; }
        /// <summary>
        /// 机器人QQ
        /// </summary>
        public long RobotQQ { get; set; }
        /// <summary>
        /// 消息序号
        /// </summary>
        public string MsgNum { get; set; }
        /// <summary>
        /// 获取当前消息的来源群组号
        /// </summary>
        public long FromGroup { get; private set; }

        /// <summary>
        /// 获取当前消息的来源QQ号
        /// </summary>
        public long FromQQ { get; private set; }

        /// <summary>
        /// 获取当前消息的消息内容
        /// </summary>
        public string Message { get; private set; }

        /// <summary>
        /// 是否截断
        /// </summary>
        public MessageTypeEnum Handler { get; set; }


        /// <summary>
        /// 初始化 <see cref="XqGroupMessageModel"/> 类的一个新实例
        /// </summary>
        /// <param name="msgId">消息ID</param>
        /// <param name="fromGroup">来源群组</param>
        /// <param name="fromQQ">来源QQ</param>
        /// <param name="anonymous">来源匿名</param>
        /// <param name="msg">消息</param>
        public XqGroupPrivateMessageModel(string msgId, string msgNum, long robotQQ, long fromGroup, long fromQQ, string msg)
        {
            this.MsgId = msgId;
            this.MsgNum = msgNum;
            this.RobotQQ = robotQQ;
            this.FromGroup = fromGroup;
            this.FromQQ = fromQQ;
            this.Message = msg;
            this.Handler = (MessageTypeEnum)0;
        }
    }
}
