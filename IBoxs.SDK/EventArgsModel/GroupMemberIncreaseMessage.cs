﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IBoxs.SDK.Enum;

namespace IBoxs.SDK.EventArgsModel
{
    /// <summary>
    /// 群成员增加事件消息
    /// </summary>
    public class XqGroupMemberIncreaseMessage
    {
        /// <summary>
        /// 机器人QQ
        /// </summary>
        public long RobotQQ { get; private set; }
        /// <summary>
        /// 来源群
        /// </summary>
        public long FromGroup { get; private set; }
        /// <summary>
        /// 来源QQ
        /// </summary>
        public long FromQQ { get; private set; }
        /// <summary>
        /// 被操作QQ
        /// </summary>
        public long BeiQQ { get; private set; }
        /// <summary>
        /// 减少原因
        /// </summary>
        public Enum.GroupMemberIncreaseEnum Type { get; private set; }

        /// <summary>
        /// 是否截断
        /// </summary>
        public MessageTypeEnum Handler { get; set; }


        public XqGroupMemberIncreaseMessage(long robotQQ, long fromGroup, long fromQQ, long BeiQQ, int type)
        {
            this.RobotQQ = robotQQ;
            this.FromGroup = fromGroup;
            this.FromQQ = FromQQ;
            this.BeiQQ = BeiQQ;
            this.Type = (Enum.GroupMemberIncreaseEnum)type;
            this.Handler = (MessageTypeEnum)0;
        }
    }
}
