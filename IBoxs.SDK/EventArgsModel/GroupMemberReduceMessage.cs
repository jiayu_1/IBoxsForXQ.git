﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IBoxs.SDK.Enum;

namespace IBoxs.SDK.EventArgsModel
{
    /// <summary>
    /// 群成员减少事件
    /// </summary>
    public class XqGroupMemberReduceMessage
    {
        /// <summary>
        /// 机器人QQ
        /// </summary>
        public long RobotQQ { get; private set; }
        /// <summary>
        /// 来源群
        /// </summary>
        public long FromGroup { get; private set; }
        /// <summary>
        /// 来源QQ
        /// </summary>
        public long FromQQ { get; private set; }
        /// <summary>
        /// 被操作QQ
        /// </summary>
        public long BeiQQ { get; private set; }
        /// <summary>
        /// 减少原因
        /// </summary>
        public SDK.Enum.GroupMemberReduce Reson { get; set; }

        /// <summary>
        /// 是否截断
        /// </summary>
        public MessageTypeEnum Handler { get; set; }

        public XqGroupMemberReduceMessage(long robotQQ, long fromGroup, long fromQQ, long BeiQQ, int type)
        {
            this.RobotQQ = robotQQ;
            this.FromGroup = fromGroup;
            this.FromQQ = FromQQ;
            this.BeiQQ = BeiQQ;
            this.Reson = (Enum.GroupMemberReduce)type;
            this.Handler = (MessageTypeEnum)0;
        }
    }
}
