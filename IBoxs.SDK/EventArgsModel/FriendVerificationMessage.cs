﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IBoxs.SDK.Enum;

namespace IBoxs.SDK.EventArgsModel
{
    /// <summary>
    /// 好友验证消息
    /// </summary>
    public class XqFriendVerificationMessage
    {
        /// <summary>
        /// 机器人QQ
        /// </summary>
        public long RobotQQ { get; private set; }
        /// <summary>
        /// 来源QQ
        /// </summary>
        public long FromQQ { get; private set; }
        /// <summary>
        /// 验证类型
        /// </summary>
        public Enum.FriendSignReplyEnum Type { get; private set; }
        /// <summary>
        /// 附加消息
        /// </summary>
        public string Note { get; private set; }

        /// <summary>
        /// 是否截断
        /// </summary>
        public MessageTypeEnum Handler { get; set; }

        /// <summary>
        /// 好友验证消息
        /// </summary>
        /// <param name="RobotQQ"></param>
        /// <param name="FromQQ"></param>
        /// <param name="Type"></param>
        /// <param name="Note"></param>
        public XqFriendVerificationMessage(long RobotQQ, long FromQQ, int Type, string Note)
        {
            this.RobotQQ = RobotQQ;
            this.FromQQ = FromQQ;
            this.Type = (Enum.FriendSignReplyEnum)Type;
            this.Note = Note;
            this.Handler = (MessageTypeEnum)0;
        }
    }
}
