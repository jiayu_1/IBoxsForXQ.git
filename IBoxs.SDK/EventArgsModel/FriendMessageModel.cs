﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IBoxs.SDK.Enum;

namespace IBoxs.SDK.EventArgsModel
{
   public class XqFriendMessageModel
    {
        /// <summary>
        /// 获取或设置一个值, 表示当前事件所产生消息的唯一编号, 可用于撤回消息
        /// </summary>
        public string MsgId { get; set; }
        /// <summary>
        /// 机器人QQ
        /// </summary>
        public long RobotQQ { get; set; }
        /// <summary>
        /// 消息序号
        /// </summary>
        public string MsgNum { get; set; }

        /// <summary>
        /// 获取当前消息的来源QQ号
        /// </summary>
        public long FromQQ { get; private set; }

        /// <summary>
        /// 获取当前消息的消息内容
        /// </summary>
        public string Message { get; private set; }

        /// <summary>
        /// 是否截断
        /// </summary>
        public MessageTypeEnum Handler { get; set; }

        public XqFriendMessageModel(string msgId, string msgNum, long robotQQ, long fromQQ, string msg)
        {
            this.MsgId = msgId;
            this.MsgNum = msgNum;
            this.RobotQQ = robotQQ;
            this.FromQQ = fromQQ;
            this.Message = msg;
            this.Handler = (MessageTypeEnum)0;
        }
    }
}
