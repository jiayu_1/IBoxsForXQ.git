﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IBoxs.SDK.Enum;

namespace IBoxs.SDK.EventArgsModel
{
    /// <summary>
    /// 某人已被邀请进群消息
    /// </summary>
   public class XqAddGroupMessage
    {
        /// <summary>
        /// 机器人QQ
        /// </summary>
        public long RobotQQ { get; private set; }

        /// <summary>
        /// 来源群
        /// </summary>
        public long FromGroup { get; private set; }

        /// <summary>
        /// 来源QQ
        /// </summary>
        public long FromQQ { get; private set; }

        /// <summary>
        /// 被操作QQ
        /// </summary>
        public long BeiQQ { get; private set; }

        /// <summary>
        /// 原始Seq
        /// </summary>
        public string Seq { get; private set; }
        /// <summary>
        /// 是否截断
        /// </summary>
        public MessageTypeEnum Handler { get; set; }

        /// <summary>
        /// 初始化 <see cref="XqGroupMessageModel"/> 类的一个新实例
        /// </summary>
        /// <param name="msgId">消息ID</param>
        /// <param name="fromGroup">来源群组</param>
        /// <param name="fromQQ">来源QQ</param>
        /// <param name="anonymous">来源匿名</param>
        /// <param name="msg">消息</param>
        public XqAddGroupMessage(long robotQQ,long fromGroup, long fromQQ,long BeiQQ, string Seq)
        {
            this.RobotQQ = robotQQ;
            this.FromGroup = fromGroup;
            this.FromQQ = fromQQ;
            this.BeiQQ = BeiQQ;
            this.Seq = Seq;
            this.Handler = (MessageTypeEnum)0;
        }
    }
}
