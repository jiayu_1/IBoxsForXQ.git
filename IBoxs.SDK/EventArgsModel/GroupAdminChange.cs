﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IBoxs.SDK.Enum;

namespace IBoxs.SDK.EventArgsModel
{
    /// <summary>
    /// 管理员变动消息
    /// </summary>
   public class XqGroupAdminChange
    {
        /// <summary>
        /// 机器人QQ
        /// </summary>
        public long RobotQQ { get; private set; }
        /// <summary>
        /// 来源群
        /// </summary>
        public long FromGroup { get; private set; }
        /// <summary>
        /// 来源QQ
        /// </summary>
        public long FromQQ { get; private set; }
        /// <summary>
        /// 被操作QQ
        /// </summary>
        public long BeiQQ { get; private set; }
        /// <summary>
        /// 变化类型
        /// </summary>
        public SDK.Enum.GroupAdminMessageEnum Type { get;private set; }

        /// <summary>
        /// 是否截断
        /// </summary>
        public MessageTypeEnum Handler { get; set; }


        public XqGroupAdminChange(long robotQQ, long fromGroup, long fromQQ, long BeiQQ, int type)
        {
            this.RobotQQ = robotQQ;
            this.FromGroup = FromGroup;
            this.FromQQ = FromQQ;
            this.BeiQQ = BeiQQ;
            this.Type = (Enum.GroupAdminMessageEnum)type;
            this.Handler = (MessageTypeEnum)0;
        }
    }
}
