﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBoxs.SDK.Enum
{
    /// <summary>
    /// 群成员减少类型
    /// </summary>
    public enum GroupMemberReduce
    {
        /// <summary>
        /// 主动退出群
        /// </summary>
        SignOut=201,
        /// <summary>
        /// 被管理员移除群
        /// </summary>
        Delete=202
    }
}
