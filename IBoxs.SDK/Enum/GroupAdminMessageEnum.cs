﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBoxs.SDK.Enum
{
    /// <summary>
    /// 群管理员变化类型
    /// </summary>
    public enum GroupAdminMessageEnum
    {
        /// <summary>
        /// 成为管理
        /// </summary>
        Become=210,
        /// <summary>
        /// 被撤销管理
        /// </summary>
        Dismissal=211
    }
}
