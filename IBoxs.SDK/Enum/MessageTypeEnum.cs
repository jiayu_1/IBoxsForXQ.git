﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBoxs.SDK.Enum
{
    /// <summary>
    /// 消息处理类型
    /// </summary>
    public enum MessageTypeEnum
    {
        /// <summary>
        /// 忽略
        /// </summary>
        Ignore=0,
        /// <summary>
        /// 继续
        /// </summary>
        Continue=1,
        /// <summary>
        /// 拦截
        /// </summary>
        Intercept=2
    }
}
