﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBoxs.SDK.Enum
{
    /// <summary>
    /// 转账来源
    /// </summary>
    public enum TransferEnum
    {
        /// <summary>
        /// 好友
        /// </summary>
        Friend=1,
        /// <summary>
        /// 群临时会话
        /// </summary>
        GroupPrivate=4,
        /// <summary>
        /// 讨论组临时会话
        /// </summary>
        Discuss=5
    }
}
