﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBoxs.SDK.Enum
{
    /// <summary>
    /// 撤回消息子类型枚举
    /// </summary>
    public enum WithdrawEnum
    {
        /// <summary>
        /// 来自群
        /// </summary>
        Group=2,
        /// <summary>
        /// 来自好友
        /// </summary>
        Friend=1
    }
}
