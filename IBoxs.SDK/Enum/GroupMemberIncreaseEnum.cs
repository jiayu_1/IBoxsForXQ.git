﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBoxs.SDK.Enum
{
    /// <summary>
    /// 群成员增加类型
    /// </summary>
    public enum GroupMemberIncreaseEnum
    {
        /// <summary>
        /// 被批准加入群
        /// </summary>
        Approval=212,
        /// <summary>
        /// 被邀请加入群
        /// </summary>
        Invitation=215
    }
}
