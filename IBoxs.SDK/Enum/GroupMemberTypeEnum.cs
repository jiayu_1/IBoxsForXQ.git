﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBoxs.SDK.Enum
{
    public enum GroupMemberTypeEnum
    {
        /// <summary>
        /// 群主
        /// </summary>
        Leader=2,
        /// <summary>
        /// 管理员
        /// </summary>
        Manager=1,
        /// <summary>
        /// 普通成员
        /// </summary>
        None=0
    }
}
