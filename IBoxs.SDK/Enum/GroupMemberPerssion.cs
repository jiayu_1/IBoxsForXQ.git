﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBoxs.SDK.Enum
{
    /// <summary>
    /// 群管理权限
    /// </summary>
    public enum GroupMemberPerssion
    {
        /// <summary>
        /// 群主
        /// </summary>
        Leader = 2,
        /// <summary>
        /// 管理员
        /// </summary>
        Person=1,
        /// <summary>
        /// 群成员
        /// </summary>
        None=0
    }
}
