﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBoxs.SDK.Enum
{
    /// <summary>
    /// 好友验证回复类型枚举
    /// </summary>
    public enum FriendSignReplyEnum
    {
        /// <summary>
        /// 被单向加为好友
        /// </summary>
        One_Way = 100,

        /// <summary>
        /// 请求加为好友
        /// </summary>
        RequestAddFriend = 101,
        /// <summary>
        /// 被同意加为好友
        /// </summary>
        Agree = 102,
        /// <summary>
        /// 被拒绝加为好友
        /// </summary>
        Refuse = 103,
        /// <summary>
        /// 被删除好友
        /// </summary>
        Delete = 104
    }
}
