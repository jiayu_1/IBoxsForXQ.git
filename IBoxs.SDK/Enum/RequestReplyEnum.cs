﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBoxs.SDK.Enum
{
    /// <summary>
    /// 请求处理类型
    /// </summary>
    public enum RequestReplyEnum
    {
        /// <summary>
        /// 忽略
        /// </summary>
        ignore=30,
        /// <summary>
        /// 拒绝
        /// </summary>
        refuse=20,
        /// <summary>
        /// 同意
        /// </summary>
        agree=10,
        /// <summary>
        /// 同意单向
        /// </summary>
        one_way=30
    }
}
