﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBoxs.SDK.Enum
{
    /// <summary>
    /// 机器人QQ状态
    /// </summary>
    public enum RobotQQStautsEnum
    {
        /// <summary>
        /// QQ登录完成
        /// </summary>
        LoginSuccess = 1101,
        /// <summary>
        /// 被手动离线
        /// </summary>
        Manual = 1102,
        /// <summary>
        /// 被强制离线
        /// </summary>
        Force = 1103,
        /// <summary>
        /// 掉线
        /// </summary>
        Off = 1104,
        /// <summary>
        /// QQ二次数据缓存完成
        /// </summary>
        Cache = 1105,
        /// <summary>
        /// QQ登录需要令牌
        /// </summary>
        Token = 1106,
        /// <summary>
        /// QQ登录需要短信
        /// </summary>
        Sms = 1107,
        /// <summary>
        /// 登录失败
        /// </summary>
        Fail = 1108
    }
}
