﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using IBoxs.SDK.EventArgsModel;

namespace IBoxs.Core.Event
{
    /// <summary>
    /// 应用状态事件
    /// </summary>
    class Event_AppStauts
    {
        /// <summary>
        /// 框架即将关闭
        /// </summary>
        public static void FrameDisable()
        {

        }
        /// <summary>
        /// 插件启用
        /// </summary>
        public static void AppEnble()
        {

        }
        /// <summary>
        /// 插件被禁用
        /// </summary>
        public static void AppDisable()
        {

        }

        /// <summary>
        /// 用户点击了设置按钮
        /// </summary>
        public static void CallMenu()
        {

        }
        /// <summary>
        /// 机器人QQ状态变化消息
        /// </summary>
        /// <param name="e"></param>
        public static void RobotStatus(XqRobotStautsMessage e)
        {

        }
    }
}
