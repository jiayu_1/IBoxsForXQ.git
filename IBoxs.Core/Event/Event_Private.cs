﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IBoxs.SDK.EventArgsModel;
using System.Data;
using System.IO;

namespace IBoxs.Core.Event
{
    /// <summary>
    /// 私聊或好友事件
    /// </summary>
    class Event_Private
    {
        /// <summary>
        /// 收到好友消息
        /// </summary>
        /// <param name="e"></param>
        public static void FriendsMessage(XqFriendMessageModel e)
        {
            if (e.Handler != SDK.Enum.MessageTypeEnum.Intercept)
                Common.CqApi.SendPrivateMessage(e.RobotQQ, e.FromQQ, "你发送了这样的消息：" + e.Message);
        }
        /// <summary>
        /// 群私聊消息
        /// </summary>
        /// <param name="e"></param>
        public static void GroupPrivateMessage(XqGroupPrivateMessageModel e)
        {

        }
        /// <summary>
        /// 在线临时消息
        /// </summary>
        /// <param name="e"></param>
        public static void TemporaryMessage(XqFriendMessageModel e)
        {

        }
        /// <summary>
        /// 收到好友验证回复信息
        /// </summary>
        /// <param name="e"></param>
        public static void FriendVerificationReply(XqFriendVerificationMessage e)
        {

        }

        /// <summary>
        /// 转账消息
        /// </summary>
        /// <param name="e"></param>
        public static void TransferMessage(XqTransferModel e)
        {

        }
        /// <summary>
        /// 消息被撤回事件
        /// </summary>
        /// <param name="e"></param>
        public static void WithdrawMessage(XqWithDrawMessage e)
        {

        }
    }
}
