﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IBoxs.SDK.EventArgsModel;
using System.Data;

namespace IBoxs.Core.Event
{
    /// <summary>
    /// 群相关事件
    /// </summary>
    class Event_Group
    {
        /// <summary>
        /// 收到群消息
        /// </summary>
        /// <param name="e"></param>
        public static void GroupMessage(XqGroupMessageModel e)
        {

        }
        /// <summary>
        /// 收到申请进群申请
        /// </summary>
        /// <param name="e"></param>
        public static void AddGroupRequest(XqAddGroupRequest e)
        {

        }
        /// <summary>
        /// 机器人收到进群邀请
        /// </summary>
        /// <param name="e"></param>
        public static void RobotAddGroupRequest(XqAddGroupRequest e)
        {

        }
        /// <summary>
        /// 某人已被邀请入群消息
        /// </summary>
        /// <param name="e"></param>
        public static void InvSuccess(XqAddGroupMessage e)
        {

        }
        /// <summary>
        /// 群成员增加事件
        /// </summary>
        /// <param name="e"></param>
        public static void GroupMemberIncrease(XqGroupMemberIncreaseMessage e)
        {

        }
        /// <summary>
        /// 群成员减少事件
        /// </summary>
        /// <param name="e"></param>
        public static void GroupMemberReduce(XqGroupMemberReduceMessage e)
        {

        }
        /// <summary>
        /// 群管理员变动事件
        /// </summary>
        /// <param name="e"></param>
        public static void GroupAdminChange(XqGroupAdminChange e)
        {

        }
    }
}
