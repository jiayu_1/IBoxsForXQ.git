﻿using System;
using System.Linq;
using IBoxs.Core.Export;
using IBoxs.SDK.Models;

namespace IBoxs.Core
{
    public class PluginMain
    {
        public static AppInfo Info() => new AppInfo()
        {
            /* 打个小广告：
             * 1.格子吧一站式软件授权服务系统：https://auth.itgz8.com
             * 2.为您提供插件收费、授权管理、卡密授权一站式服务
             */
            name = "先驱框架C# SDK",
            author = "IT格子",
            desc = "先驱框架C# SDK",
            pver = "1.0.0"
        };
    }
}