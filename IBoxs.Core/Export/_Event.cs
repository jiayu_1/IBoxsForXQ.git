﻿using System;
using System.Collections.Generic;
using HuajiTech.UnmanagedExports;
using IBoxs.SDK.EventArgsModel;
using IBoxs.SDK.Enum;

namespace IBoxs.Core.Export
{
    public class _Event
    {
        [DllExport]
        public static int
            XQ_Event(string robotQQ, int eventType, int extraType, string from, string fromQQ, string targetQQ, string content, string index, string msgid, string udpmsg, string unix, int p) => Event(robotQQ, eventType, extraType, from, fromQQ, targetQQ, content, index, msgid, udpmsg, unix, p);

        /// <summary>
        /// 分发事件
        /// </summary>
        /// <param name="robotQQ">机器人QQ</param>
        /// <param name="eventType">接收到消息类型，该类型可在常量表中查询具体定义，此处仅列举： -1 未定义事件 0,在线状态临时会话 1,好友信息 2,群信息 3,讨论组信息 4,群临时会话 5,讨论组临时会话 6,财付通转账 7,好友验证回复会话</param>
        /// <param name="extraType">此参数在不同消息类型下，有不同的定义，暂定：接收财付通转账时 1为好友 4为群临时会话 5为讨论组临时会话    有人请求入群时，不良成员这里为1</param>
        /// <param name="from">此消息的来源，如：群号、讨论组ID、临时会话QQ、好友QQ等</param>
        /// <param name="fromQQ">主动发送这条消息的QQ，踢人时为踢人管理员QQ</param>
        /// <param name="targetQQ">被动触发的QQ，如某人被踢出群，则此参数为被踢出人QQ</param>
        /// <param name="content">此参数有多重含义，常见为：对方发送的消息内容，但当XQ_消息类型为 某人申请入群，则为入群申请理由</param>
        /// <param name="index">此参数暂定用于消息回复，消息撤回</param>
        /// <param name="msgid">此参数暂定用于消息回复，消息撤回</param>
        /// <param name="udpmsg">UDP收到的原始信息，特殊情况下会返回JSON结构（入群事件时，这里为该事件seq）</param>
        /// <param name="unix">接受到消息的时间戳</param>
        /// <param name="p">此参数用于插件加载拒绝理由  用法：写到内存（“拒绝理由”，XQ_回传文本指针，255） ‘最大写入字节数量为255超过此长度可能导致插件异常崩溃</param>
        /// <returns></returns>
        public static int Event(string robotQQ, int eventType, int extraType, string from, string fromQQ, string targetQQ, string content, string index, string msgid, string udpmsg, string unix, int p)
        {
            try
            {
                Common.CqApi.RobotQQ = Convert.ToInt64(robotQQ);
            }
            catch { }
            MessageTypeEnum returnHandler = (MessageTypeEnum)0;
            switch (eventType)
            {
                case -1: break;  //未定义事件
                case 0://在线临时会话
                    XqFriendMessageModel e0 = new XqFriendMessageModel(msgid, index, Convert.ToInt64(robotQQ), Convert.ToInt64(from), content);
                    IBoxs.Core.Event.Event_Private.TemporaryMessage(e0);returnHandler = e0.Handler; break;
                case 1://好友消息
                    XqFriendMessageModel e1 = new XqFriendMessageModel(msgid, index, Convert.ToInt64(robotQQ), Convert.ToInt64(from), content);
                    IBoxs.Core.Event.Event_Private.FriendsMessage(e1); returnHandler = e1.Handler; break;
                case 2://群消息
                    XqGroupMessageModel e2 = new XqGroupMessageModel(msgid, index, Convert.ToInt64(robotQQ), Convert.ToInt64(from), Convert.ToInt64(fromQQ), content);
                    IBoxs.Core.Event.Event_Group.GroupMessage(e2); returnHandler = e2.Handler; break;
                case 4://群私聊消息
                    XqGroupPrivateMessageModel e4 = new XqGroupPrivateMessageModel(msgid, index, Convert.ToInt64(robotQQ), Convert.ToInt64(from), Convert.ToInt64(fromQQ), content);
                    IBoxs.Core.Event.Event_Private.GroupPrivateMessage(e4); returnHandler = e4.Handler; break;
                case 6://转账消息
                    XqTransferModel e6 = new XqTransferModel(Convert.ToInt64(robotQQ), Convert.ToInt64(from), extraType, udpmsg);
                    IBoxs.Core.Event.Event_Private.TransferMessage(e6); returnHandler = e6.Handler; break;
                case 7://好友验证消息回复
                case 9://消息被撤回
                    XqWithDrawMessage e9 = new XqWithDrawMessage(Convert.ToInt64(robotQQ), Convert.ToInt64(from), Convert.ToInt64(targetQQ), extraType, msgid, index);
                    IBoxs.Core.Event.Event_Private.WithdrawMessage(e9); returnHandler = e9.Handler; break;
                case 100:
                case 101:
                case 102:
                case 103:
                case 104:///收到好友验证消息
                    XqFriendVerificationMessage e104 = new XqFriendVerificationMessage(Convert.ToInt64(robotQQ), Convert.ToInt64(from), eventType, content);
                    Core.Event.Event_Private.FriendVerificationReply(e104); returnHandler = e104.Handler; break;
                case 201:
                case 202://群成员减少
                    XqGroupMemberReduceMessage e201 = new XqGroupMemberReduceMessage(Convert.ToInt64(robotQQ), Convert.ToInt64(from), Convert.ToInt64(fromQQ), Convert.ToInt64(targetQQ), extraType);
                    Core.Event.Event_Group.GroupMemberReduce(e201); returnHandler = e201.Handler; break;
                case 210:
                case 211:
                    XqGroupAdminChange e211 = new XqGroupAdminChange(Convert.ToInt64(robotQQ), Convert.ToInt64(from), Convert.ToInt64(fromQQ), Convert.ToInt64(targetQQ), extraType);
                    Core.Event.Event_Group.GroupAdminChange(e211); returnHandler = e211.Handler; break;
                case 212:
                case 215://群成员增加
                    XqGroupMemberIncreaseMessage e205 = new XqGroupMemberIncreaseMessage(Convert.ToInt64(robotQQ), Convert.ToInt64(from), Convert.ToInt64(fromQQ), Convert.ToInt64(targetQQ), extraType);
                    Core.Event.Event_Group.GroupMemberIncrease(e205); returnHandler = e205.Handler; break;
                case 213:  //进群申请
                    XqAddGroupRequest e213 = new XqAddGroupRequest(Convert.ToInt64(robotQQ), Convert.ToInt64(from), Convert.ToInt64(fromQQ), content, udpmsg, extraType);
                    IBoxs.Core.Event.Event_Group.AddGroupRequest(e213); returnHandler = e213.Handler; break;
                case 214://机器人收到进群邀请
                    XqAddGroupRequest e214 = new XqAddGroupRequest(Convert.ToInt64(robotQQ), Convert.ToInt64(from), Convert.ToInt64(fromQQ), content, udpmsg, 0);
                    IBoxs.Core.Event.Event_Group.RobotAddGroupRequest(e214); returnHandler = e214.Handler; break;
                case 219://某人已被邀请进群
                    XqAddGroupMessage e215 = new XqAddGroupMessage(Convert.ToInt64(robotQQ), Convert.ToInt64(from), Convert.ToInt64(fromQQ), Convert.ToInt64(targetQQ), udpmsg);
                    IBoxs.Core.Event.Event_Group.InvSuccess(e215); returnHandler = e215.Handler; break;
                case 12001://用户启用本插件
                    IBoxs.Core.Event.Event_AppStauts.AppEnble();  break;
                case 12002://用户禁用本插件
                    IBoxs.Core.Event.Event_AppStauts.AppDisable();  break;
                case 10001://框架即将关闭
                    IBoxs.Core.Event.Event_AppStauts.FrameDisable();  break;
                case 1101:
                case 1102:
                case 1103:
                case 1104:
                case 1105:
                case 1106:
                case 1107://机器人状态变化事件
                case 1108:
                    XqRobotStautsMessage e1104 = new XqRobotStautsMessage(Convert.ToInt64(robotQQ), extraType);
                    Core.Event.Event_AppStauts.RobotStatus(e1104); returnHandler = e1104.Handler; break;
            }
            return (int)returnHandler;
        }

        [DllExport]
        public static int XQ_SetUp() => Menu();

        public static int Menu()
        {
            IBoxs.Core.Event.Event_AppStauts.CallMenu();
            return 0;
        }
    }
}