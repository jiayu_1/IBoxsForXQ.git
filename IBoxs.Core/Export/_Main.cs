﻿using System;
using System.IO;
using IBoxs.SDK;

namespace IBoxs.Core.Export
{
    internal class _Main
    {
        public static CqApi MainXQAPI = new CqApi();

        public static void Log(string log)
        {
            if (!Directory.Exists("XQNetLogs"))
            {
                Directory.CreateDirectory("XQNetLogs");
            }

            File.AppendAllText("XQNetLogs\\log.txt", log + "\n");
        }
    }
}