﻿using System;
using System.IO;
using HuajiTech.UnmanagedExports;

namespace IBoxs.Core.Export
{
    public class _Create
    {
        [DllExport]
        public static string XQ_Create(string frameworkversion) => CreateMain();

        public static string CreateMain()
        {
            var info = PluginMain.Info();
            _Main.MainXQAPI.SetAppInfo(PluginMain.Info());
            _Main.MainXQAPI.AppDirectory = Directory.GetCurrentDirectory() + "\\Config" + "\\" + info.name + "\\";
            if (!Directory.Exists(_Main.MainXQAPI.AppDirectory))
                Directory.CreateDirectory(_Main.MainXQAPI.AppDirectory);
            return info.ToJson();
        }
    }
}